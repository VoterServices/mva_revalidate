﻿Imports MDSBE.Common.DataAccess

Public Class SaveSignature
    Inherits AbstractSave

    Private Soundex As String
    Private Signature As Byte()

    Public Sub New(ByVal ConnString As String, ByVal ProviderName As String, ByVal sdx As String, ByVal sign As Byte())
        MyBase.New(ConnString, ProviderName, "SaveSignature")
        Me.Soundex = sdx
        Me.Signature = sign
    End Sub

    Protected Overrides Sub SetCommandText()
        MyBase.Command.CommandText = "MVA_VERIFICATION.ins_MVASignature"
    End Sub

    Protected Overrides Sub SetParameters()
        MyBase.AddInParameter("in_soundex", Me.Soundex)
        MyBase.AddInParameter("in_signature", Me.Signature)
    End Sub
End Class
