﻿Imports MDSBE.Common.DataAccess

Public Class UpdateRecord
    Inherits AbstractSave

    Private VRA_ID As Integer
    Private MVA_Response As Integer

    Public Sub New(ByVal ConnString As String, ByVal ProviderName As String, ByVal vraid As Integer, ByVal response As Integer)
        MyBase.New(ConnString, ProviderName, "UpdateRecord")
        Me.VRA_ID = vraid
        Me.MVA_Response = response
    End Sub

    Protected Overrides Sub SetCommandText()
        MyBase.Command.CommandText = "VRA.upd_UnprocessedApp_MVADown"
    End Sub

    Protected Overrides Sub SetParameters()
        MyBase.AddInParameter("in_vraid", Me.VRA_ID)
        MyBase.AddInParameter("in_mva_response", Me.MVA_Response)
    End Sub
End Class
