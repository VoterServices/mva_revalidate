﻿Imports System.Net
Imports MDSBE.Common.ErrorHandling
Imports System.Text
Imports System.Globalization
Imports System.Net.Http
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Module Module1

    Sub Main()
        Dim log As ErrorLogger = Nothing
        Dim message As String = ""
        Dim mvaresponse As Int32

        'set up variables
        Dim LogPath As String = System.Configuration.ConfigurationManager.AppSettings("LogPath").ToString
        log = New ErrorLogger("MVA_ReValidate", LogPath)

        Dim connString As String = System.Configuration.ConfigurationManager.ConnectionStrings("connection").ToString()
        Dim providerName As String = System.Configuration.ConfigurationManager.ConnectionStrings("connection").ProviderName

        Dim endpoint As String = System.Configuration.ConfigurationManager.AppSettings("endpoint").ToString()
        Dim port As String = System.Configuration.ConfigurationManager.AppSettings("port").ToString()


        'get records from database
        Dim srch As New GetRecords(connString, providerName)
        srch.Execute()

        'validate each record and then update the database
        For Each rec As MVA_Verify_Object In srch.Result

            If String.IsNullOrEmpty(rec.DL) Then
                mvaresponse = 7
            Else
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim certificate As X509Certificate2 = New X509Certificate2("C:\SSLCerts\olvr-web-mvaapi.pfx", "C33D7kdnP6ga5N9")
                Dim url As String = ""

                url = "https://mymva.maryland.gov:5443/MDP/WebServices/SBERST/GetSig/?CustomerID=" & rec.DL & "&DOB=" & rec.DOB & "&IssuanceDate=" & rec.DL_IssueDate _
                    & "&Last4SSN=" & rec.SSN4 & "&LastName=" & rec.Last_Name.ToUpper

                Dim myReq As HttpWebRequest
                Dim myResp As HttpWebResponse
                Dim myReader As StreamReader

                myReq = CType(HttpWebRequest.Create(url), HttpWebRequest)
                myReq.Method = "GET"
                myReq.ContentType = "application/json"
                myReq.Accept = "application/json"
                myReq.ClientCertificates.Add(certificate)

                myResp = CType(myReq.GetResponse, HttpWebResponse)
                myReader = New System.IO.StreamReader(myResp.GetResponseStream)
                Dim rawresp As String
                rawresp = myReader.ReadToEnd()

                Dim jResults As JObject = JObject.Parse(rawresp)
                Dim signature() As Byte
                signature = System.Convert.FromBase64String(CStr(jResults.Item("Signature")))
                mvaresponse = CInt(jResults.Item("VoterSearchResult").ToString)


                'now read the signature if record is found
                If mvaresponse = 3 Then
                    'save the signature to database
                    Dim save_sign As New SaveSignature(connString, providerName, rec.DL, signature)
                    save_sign.Execute()

                Else

                End If
            End If

            'update the record with mva response
            Dim update As New UpdateRecord(connString, providerName, rec.VRA_ID, mvaresponse)
            update.Execute()

        Next

    End Sub

End Module
