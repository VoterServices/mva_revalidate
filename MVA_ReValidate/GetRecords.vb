﻿Imports MDSBE.Common.DataAccess

Public Class MVA_Verify_Object
    Public Sub New()

    End Sub

    Property Last_Name As String
    Property DOB As String
    Property SSN4 As String
    Property DL_IssueDate As String
    Property DL As String
    Property VRA_ID As Integer
End Class

Public Class GetRecords
    Inherits AbstractSearch

    Public Property Result As List(Of MVA_Verify_Object)

    Public Sub New(ByVal connString As String, ByVal providerName As String)
        MyBase.New(connString, providerName, "Get_MVADown_Records")
    End Sub

    Protected Overrides Sub fill(ByRef rdr As IDataReader)
        Result = New List(Of MVA_Verify_Object)

        Try
            While rdr.Read()
                Dim rec As New MVA_Verify_Object
                rec.DL = DataCleanUpHelper.NullScrubber(Of String)(rdr("DRIVERS_LICENSE"))
                rec.DL_IssueDate = DataCleanUpHelper.NullScrubber(Of String)(rdr("DL_ISSUE_DATE"))
                rec.DOB = DataCleanUpHelper.NullScrubber(Of String)(rdr("DATE_OF_BIRTH"))
                rec.Last_Name = DataCleanUpHelper.NullScrubber(Of String)(rdr("LAST_NAME"))
                rec.SSN4 = DataCleanUpHelper.NullScrubber(Of String)(rdr("SSN4"))
                rec.VRA_ID = DataCleanUpHelper.NullScrubber(Of Integer)(rdr("VRA_ID"))
                Result.Add(rec)
            End While

        Catch ex As Exception
            Dim ex1 As New Exception("GetRecords->fill", ex)
            Throw ex1
        End Try
    End Sub

    Protected Overrides Sub SetCommandText()
        MyBase.Command.CommandText = "VRA.get_UnprocessedApps_MVADown"
    End Sub

    Protected Overrides Sub SetParameters()

    End Sub
End Class
